public class Portero extends Equipo
{
    public Portero(int numUniforme, String nombre, String posicion, int minutosJugados) {
        super(numUniforme, nombre, posicion, minutosJugados);
        
    }

    public void mostrar(){
        System.out.println("Numero del uniforme: " + getNumUniforme());
        System.out.println("Nombre del jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Minutos jugados: " + getMinutosJugados() + "''");               
    }
}