public class Jugador extends Equipo
{
    private int goles;
    public Jugador(int numUniforme, String nombre, String posicion, int minutosJugados, int goles) {
        super(numUniforme, nombre, posicion, minutosJugados);
        this.goles = goles;
    }
    
    public int getGoles(){
      return goles;
    }
    
    public void setGoles(int goles){
      this.goles = goles;
    }
    
    public void mostrar(){
        System.out.println("Numero del uniforme: " + getNumUniforme());
        System.out.println("Nombre del jugador: " + getNombre());
        System.out.println("Posicion: " + getPosicion());
        System.out.println("Minutos jugados: " + getMinutosJugados() + "''");
        System.out.println("Goles anotados: " + getGoles());               
    }
}