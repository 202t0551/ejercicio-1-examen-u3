public class Principal
{
     public static void main(String[] args) {

        Portero p1 = new Portero(1,"David","Portero",70);
        Jugador D1 = new Jugador(9,"Tomas","Delantero",15,8);
        Jugador M1 = new Jugador(12,"Juan","Medio",22,12);
        Jugador De1 = new Jugador(11,"Daniel","Defensa",32,7);

        //Nos muestra los detalles del objeto
        System.out.println("-------------------------------");
        p1.mostrar();        
        System.out.println("-------------------------------");
        D1.mostrar();
        System.out.println("-------------------------------");
        M1.mostrar();
        System.out.println("-------------------------------");
        De1.mostrar();
        System.out.println("-------------------------------");

    }   
}
