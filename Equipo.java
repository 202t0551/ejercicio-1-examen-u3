public class Equipo{

    private int numUniforme; 
    private String nombre;
    private String posicion;
    private int minutosJugados;
    
    public Equipo(int numUniforme, String nombre, String posicion, int minutosJugados){
    this.numUniforme = numUniforme;
    this.nombre = nombre;
    this.posicion = posicion;
    this.minutosJugados = minutosJugados;
    }
    
    
    //Metodos get
    public int getNumUniforme(){
    return numUniforme;
    }
    
    public String getNombre(){
    return nombre;
    }
    
    public String getPosicion(){
    return posicion;
    }
    
    public int getMinutosJugados(){
      return minutosJugados;
    }
    
    //Metodos Set
    public void setNumUniforme(int numUniforme){
    this.numUniforme = numUniforme;
    }
    
    
    public void setNombre(String nombre){
    this.nombre = nombre;
    }
        
    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }
    
    public void setMinutosJugados(int minutosJugados){
      this.minutosJugados = minutosJugados;
    }
}
